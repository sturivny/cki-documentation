.PHONY: default setup link-check lint serve watch hugo-serve

SHELL := /bin/bash

default: serve

podman:
	podman run \
	    --rm \
	    --interactive \
	    --tty \
	    --volume .:/data:Z \
	    --workdir /data \
	    --network host \
	    quay.io/cki/cki-tools:production

setup:
	git submodule update --init --recursive
	npm i --no-package-lock

link-check:
	./check-links.sh

lint:
	markdownlint content/

serve:
	$(MAKE) -j 2 watch hugo-serve

watch:
	if [[ -e ../stow ]]; then \
		inotifywait -qmre close_write ../stow/*/content | while read directory action file; do \
			[[ "$$file" = *.md ]] || continue; \
			target_file="$$(readlink -f "$$directory$$file")"; \
			find content -type l -print0 | while IFS= read -r -d $$'\0' source_file; do \
				if [[ "$$(readlink -f $$source_file)" = "$$target_file" ]]; then \
					mv "$$source_file" "$$source_file~~"; \
					mv "$$source_file~~" "$$source_file"; \
				fi; \
			done; \
		done; \
	fi

hugo-serve:
	hugo serve

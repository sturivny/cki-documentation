---
title: Testing CKI Builds
description: How to test CKI builds and submit results
weight: 25
---

## Ready for test messages

CKI sends ready for test messages after a kernel is built *and* after CKI
functional testing completes. This is meant to be used to trigger testing in
specialized labs for testing that is not feasible to onboard into CKI (e.g.
testing that requires a controlled environment or special hardware).

### Ready for test details

Messages are sent to `/topic/VirtualTopic.eng.cki.ready_for_test`. All labs
involved in extra testing will be notified if the topic is changed.

Onboarded labs can filter specific messages they are interested in. Some
examples include:

* Filtering on modified files to only test changes to their subsystem
* Filtering on `system/os` to only test changes to specific release
* Filtering on `cki_finished == false` to run testing in parallel
* Filtering on `cki_finished == true && status == 'success'` to e.g. only run
  performance tests if CKI functional testing passed
* Filtering on nonempty `patch_urls` to only test proposed changes instead of
  already merged ones
* A combination of the above

### Ready for test message schema

```json
{
    "ci": {
      "name": "CKI (Continuous Kernel Integration)",
      "team": "CKI",
      "docs": "https://cki-project.org/",
      "url": "https://xci32.lab.eng.rdu2.redhat.com/",
      "email": "cki-project@redhat.com",
      "irc": "#kernelci"
    },
    "run": {
      "url": "<PIPELINE_URL>"
    },
    "artifact": {
      "type": "cki-build",
      "issuer": "<PATCH_AUTHOR_EMAIL or CKI>",  # Always CKI for merge request testing
      "component": "kernel-source-package-name",
      "variant": "kernel-binary-package-name"
    },
    "system": [
      {
        "os": "<kpet_tree_family>",
        "stream": "<kpet_tree_name>"
      }
    ],
    "build_info": [
      {
        "architecture": "<ARCH>",
        "build_id": "<KCIDB build_id>",
        "kernel_package_url": "<LINK_TO_REPO>",
        "debug_kernel": bool
      },
      {
        "architecture": "<ARCH>",
        "build_id": "<KCIDB build_id>",
        "kernel_package_url": "<LINK_TO_REPO>",
        "debug_kernel": bool
      },
      ...
    ],
    "patch_urls": ["list", "of", "strings", "or", "empty", "list"],
    "merge_request": {
      "merge_request_url": "link-or-empty-string",
      "is_draft": bool,
      "subsystems": ["list", "of", "strings"],
      "jira": ["list", "of", "jira", "links"],
      "bugzilla": ["list", "of", "bz", "links"]
    },
    "branch": "name-of-branch-or-empty-string",
    "modified_files": ["list", "of", "strings", "or", "empty", "list"],
    "pipelineid": <ID>,
    "cki_finished": bool,
    # status NOT present if cki_finished is false
    "status": <"success" or "fail">,
    "category": "kernel-build",
    "namespace": "cki",
    "type": "build",
    "generated_at": "<DATETIME_STRING>",
    "version": "0.1.0"
}
```

For documentation of `kpet_tree_family` and `kpet_tree_name`, see [Configuration
options]

## Results messages

Test results should be sent back via UMB to
`/topic/VirtualTopic.eng.cki.results`. Results must be sent in KCIDB v4 format,
which has a detailed schema documented by [kcidb-io].

[Configuration options]: ../user_docs/configuration.md
[kcidb-io]: https://github.com/kernelci/kcidb-io/blob/main/kcidb_io/schema/v4.py

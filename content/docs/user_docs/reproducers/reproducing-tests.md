---
title: Reproducing and debugging test runs
linkTitle: Test results
description: How to reproduce or debug kernel testing done in the CKI pipeline
weight: 30
---

The CKI tests are not meant to be rerun locally. This is (mostly) because CKI
runs testing on specific hardware and setup to get appropriate coverage.

Most often the tests are being ran in the Red Hat [Beaker] instance, however
different labs and cloud providers are supported.

Other teams can also send their test results to CKI to propagate them to the
developers; in these cases CKI has no control over which tests are being
executed, how, and in what environment.

## Rerunning a pipeline or job via CKI bot

Use the bot testing on an opened MR in the [kernel test repository] as
described in the [bot documentation]. This also allows modifying the test to
include debug code.

## Rerunning tests in Beaker directly

This option is limited to test execution done in the Red Hat Beaker instance.

1. Grab the Beaker XML from the DataWarehouse entry of the test:

![Test entry in DataWarehouse](test-datawarehouse.png)

1. Make sure to keep the full recipe for your test without any modifications,
as sometimes the tasks that ran before the test are the reason for the failures!

1. **If you wish to execute the testing against a different kernel version**
(e.g. to check if the failures occur with base kernel), you can do so by
editing the `KPKG_URL` parameter:

    1. Search for `KPKG_URL` in the XML.
    1. Replace the part in front of `#` by a valid `yum` or `dnf` repository
       containing the kernel you wish to install.
    1. If you picked a different kernel variant to install (e.g. kernel vs
       kernel-rt, debug vs non-debug), adjust the options after `#`
       accordingly. Most of the time, you want to keep them the same.

You can also use the full `KPKG_URL` parameter (including the options) from a
different CKI Beaker XML to install a kernel from that run.

1. **Submitting the job via command line**

This step assumes the following Beaker configuration stored in
`~/.beaker_client/config`:

```text
HUB_URL = "https://beaker.engineering.redhat.com"
AUTH_METHOD = "krbv"
KRB_REALM = "REDHAT.COM"
```

Submit the job via

```bash
bkr job-submit <FILENAME>
```

You can get the `bkr` command by installing the `beaker-client` package. Full
documentation of the command is available by running `man bkr job-submit`.

1. **Submitting the job via web interface**

   1. Go to <https://beaker.engineering.redhat.com> and click on `Log in` in
      the top right corner.
   1. Open the `Scheduler` dropdown from the top left side menu. Pick the
      `New Job` option.
   1. Click on the `Browse` button and upload your file.
   1. Click on the `Submit Data` button below the `Browse` button.
   1. Click on the `Queue` button on the right side.

> **NOTE**: In some cases, the machines needed for testing are not available to
> all developers. In such case, reach out to the test maintainer(s) (their
> contact information is available on the test details in DataWarehouse) or
> [the CKI team] to get temporary access.

[Beaker]: https://beaker-project.org
[kernel test repository]: https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/
[bot documentation]: ../../operations/pipeline-bot.md
[the CKI team]: ../gitlab-mr-testing/faq/index.md#contacting-the-cki-team

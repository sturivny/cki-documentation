---
title: CKI pipeline configuration options
linkTitle: Configuration options
weight: 60
---

All pipelines [share the same code] and make decisions based on the passed
configuration options in form of trigger variables. Other tools interacting
with the pipeline may also need to do the same, one such example would be
reporter which needs to know where to send emails for the pipeline.

All the options below are in **alphabetical order**.

> Note that this list **does not contain internal pipeline variables** that are
> technically possible to be overridden but are not meant to be touched, such as
> `rpmbuild` options or `ccache` related variables.

For variables required by triggers but not needed for the pipeline run itself,
please refer to the [pipeline triggers] documentation.

## Pipeline configuration option list

> The pipeline and tooling handles both `True` and `true` (similarly with
> `False/false`). For simplicity, only capitalized values are mentioned below.

* `architectures`: Space-separated list of architectures to run the pipeline
   for, in RPM architecture format. Required in every pipeline.
* `ARTIFACT_*`: Information about where to find original pipeline artifacts if
   short pipelines are requested via `tests_only=true`. Used by the CI bot.
   MUST NOT BE USED IN PRODUCTION.
* `artifacts_mode`: Where pipeline artifacts should be stored, can be `s3` or
   `gitlab`, defaults to `s3`.
* `branch`: Target branch of the kernel repository that should be built and
   tested. Used in reporting and messaging, not in the pipeline itself.
   `commit_hash` must be provided for a correct pipeline run.
* `brew_task_id`: Brew or Koji task ID. Only applicable to Brew/Koji
   pipelines.
* `build_selftests`: `True` if any kernel selftests should be built, `False`
   otherwise. Defaults to `False`. Will cause `make kselftest-merge`
   configuration target to be called. Has to be used together with `test_debug`
   option (as the selftest config target pulls in debug options). Only
   applicable to kernels built as tarballs. **NOTE** selftests build is
   currently only supported for `debug` builds on `x86_64` architecture, other
   architectures are skipped.
* `builder_image`: Container image used for kernel building. Defaults to
   `quay.io/cki/builder-rawhide`.
* `checkout_contacts`: A string representing a JSON-encoded list of emails
   involved with the KCIDB checkout. Defaults to '[]'.
* `CKI_DEPLOYMENT_ENVIRONMENT`: `retrigger` if the pipeline is a retrigger for
   testing and should be ignored by all tooling. Defaults to `production`.
* `commit_hash`: Commit to check out from the repository. Required if a kernel
   needs to be built. For MR child pipelines, this is set to `CI_COMMIT_SHA`.
* `compiler`: Which compiler toolchain to use to build the kernel. Can be `gcc`
   or `clang`, defaults to `gcc`. Specific behavior of `clang` differs on
   supported functionality with Fedora configuration for given architecture.
   Has effect only when a kernel is built.
* `source_package_name`: Name of the kernel package. Defaults to `kernel`.
* `config_target`: `make` configuration target to use on top of Fedora
   configuration files. Defaults to `olddefconfig`. Only applicable to kernels
   built as tarballs.
* `copr_build`: Task ID of a COPR build, similar to `brew_task_id`. Only
   applicable to COPR pipelines.
* `coverage`: `True` to build a kernel with code coverage. Defaults to `False`.
* `coverage_dirs`: Space-separated list of kernel source directories to
   capture coverage data for. Has effect with `coverage=True` only.
   Default value is specified in [kpet-db] repository.
* `cover_letter`: Link to a cover letter, if patch series are being tested and
   a cover letter exists. Optional.
* `disttag_override`: Override for the RPM `%dist` macro of the builder
   container image. This is necessary if the built kernel belongs to a
   different distribution release, e.g. `.el8_3` for a RHEL 8.3 kernel built on
   the `builder-rhel` container image which has a `%dist` macro of `.el8`.
   Only applicable to kernels built as RPMs.
* `domains`: A space-separated list of regular expressions fully-matching
   names or slash-separated paths of kpet "host domains" to restrict test
   execution to. All tests targeting hosts in the matching domains will be
   executed once for each of the regular expressions. Run `kpet domain tree`
   to see available domains. If empty, tests will be executed once in all
   domains. Defaults to `available`.
* `download_separate_headers`: If `true`, download separate kernel-headers
   binary from koji. Defaults to `false`. Useful if the headers are not part of
   the base kernel package. Only applicable to Brew/Koji/COPR pipelines.
* `extra_baseline_run_set_patterns`: Space-separated string representing test
   set patterns of runs to add to the pipeline. Useful if baseline-like runs are
   needed to be executed on top of targeted testing. Defaults to an empty
   string, i.e. no extra runs.
* `force_baseline`: If `true`, MR pipeline runs won't run targeted testing but
   will pretend to be a git baseline run. Defaults to `false`.
* `git_url`: URL of the kernel repository that should be built and tested.
   Only used when a kernel is built.
* `git_url_cache_owner`: The file name for the git cache is determined as
   `owner.repo` from the git URL. Explicitly set this variable to override the
   owner component for the git cache used for `git_url`.
* `image_tag`: Container tag used for all container images in the pipeline.
   Defaults to `production`.
* `kernel_config_url`: URL to a kernel configuration to use as a base for the
   built kernel. Optional, defaults to empty string. If not provided, Fedora
   rawhide configuration files are used.
* `kernel_type`: `upstream` or `internal`. Used by DataWarehouse and KCIDB
   submitter to determine whether the kernel build is internal, or can be shared
   publicly. Defaults to `internal`.
* `kernel_version` Version part of the build NVR. Required for Brew/Koji/COPR
   builds. For builds done by CKI, it is determined in the pipeline.
* `kpet_extra_components`: A regular expression matching kpet's build
   "components" to consider included into the kernel being tested, along with
   automatically-detected ones. See the output of `kpet component list` for a
   list and description of recognized components.
* `kpet_high_cost`: Override for kpet's `--high-cost` option. Defaults to
   `triggered`. See the output of `kpet run generate --help` command for
   possible values.
* `kpet_tree_family`: Value to pass to [kpet tree selection script] to pick
   appropriate test definitions.
* `kpet_tree_name`: Automatically determined from `kpet_tree_family` and
   [kpet tree selection script] by default. Useful only as an override, e.g.
   for testing the CI pipelines. If defined, must match a kpet tree name as
   returned by `kpet tree list`.
* `KPET_ADDITIONAL_VARIABLES`: Additional environment variables that should be
   passed as variables to kpet. Only for pipelines triggered via the CI bot.
   MUST NOT BE USED IN PRODUCTION.
* `make_target`: `targz-pkg` if `make targz-pkg` should be used for building
   the kernel, `rpm` if `rpmbuild` should be used. Defaults to `rpm`. **NOTE**
   should be renamed to `build_target` in the future to reflect not all kernels
   are build using the `make` command.
* `merge_branch`: Required if `merge_tree` is specified. Name of the branch
   which should be merged into the tested kernel tree before patch application.
* `merge_tree`: Optional. Link to a kernel repository that should be merged
   into the tested kernel tree before patch application. See `merge_branch` for
   specifying the branch name.
* `merge_tree_cache_owner`: The file name for the git cache is determined as
   `owner.repo` from the git URL. Explicitly set this variable to override the
   owner component for the git cache used for `merge_tree`.
* `message_id`: Value of a `Message-ID` header, typically of the last email of
   the patch series. If specified, email report will be sent `In-Reply-To` of
   this email. If missing, email report will be sent without any threading.
* `mr_diff_base_hash`: Source branch base commit, i.e. the last commit that is
   not part of the merge request.
* `mr_id`: ID of the merge request that should be tested. Only applicable to
   kernels [living in GitLab], where changes are not submitted as email patches
   but as merge requests.
* `mr_source_branch_hash`: Source branch HEAD commit if `commit_hash` points to
   a merge commit. For MR child pipelines, this is set to
   `CI_MERGE_REQUEST_SOURCE_BRANCH_SHA`.
* `name`: Pipeline type identifier, used e.g. in tree filtering in
   [Data Warehouse]. Typically contains upstream subtree name or distribution
   release, e.g. `mainline.kernel.org` or `kernel-rt-rhel83`. **NOTE** this
   variable is deprecated as it is ambiguous and must not be used in new code.
* `native_tools`: `True` if kernel tools should be built separately from kernel
   on a native architecture. Defaults to `False`. Only available for kernels
   built as RPMs which have the required kernel features backported.
* `package_name`: Name of the kernel variant, e.g. `kernel` or `kernel-rt`.
   Required for kernels built as RPMs (both ones built in the pipeline
   and by build systems) as [boot test] uses the value. Defaults to `kernel`.
* `patch_urls`: Links to raw patches or mboxes to apply, in a form of
   space-separated string. Patches are applied using `git am`.
* `patchwork_url`: Link to a Patchwork instance, if Patchwork is the origin of
   the patches. Used to properly grab patch names, as all patches downloaded
   from patchwork are called `mbox`.
* `publish_elsewhere`: If `True`, publish the kernel repository on a Red Hat
   internal server. Defaults to `False`. Useful when testing private kernels
   which are not accessible by the test environment.
* `repo_name`: COPR repository name in a format `username/copr`, if a COPR
   build is supposed to be tested.
* `report_rules`: JSON encoded list of reporting rules for the current checkout.
* `rpmbuild_with`: Space-separated strings marking a set of `--with` options
   to be added to the `rpmbuild` command. Only applicable to kernels packaged
   as RPMs. Defaults to an empty string (no extra options added).
* `rt_kernel`: `True` if realtime kernel configuration options should be
   enabled for tarball packaged kernels. Defaults to `False`.
* `run_redhat_self_test`: `True` if the pipeline should run the kernel's
   `redhat/self-test` testsuite.  Defaults to `False`.
* `scratch`: `True` if the kernel build is a scratch build (Brew/Koji) or is
   triggered by a merge request, `False` if not. Defaults to `True`.
* `selftest_subsets`: Optional variable specifying a list of space-separated
   selftest subsets to build. If it's missing and `build_selftests` is `True`,
   CKI will build all selftest subsets. Only applicable to kernels packaged
   as tarballs.
* `send_pre_test_notification`: `True` if an email should be sent to a patch
   or build submitter before testing starts. Defaults to `False`. The email
   contains links to Beaker jobs for people to follow, thus is only useful for
   internal contributors. **NOTE** After migrating to [UPT] this notification
   will make no sense as there will be no test logs available in the Beaker
   jobs, and thus the option may end up being removed or reworked.
* `send_ready_for_test_pre`: `True` if the [UMB messenger] should send a
   "ready for test" message for parallel testing. Defaults to `False`.
* `send_ready_for_test_post`: `True` if the [UMB messenger] should send a
   "ready for test" message for post-CKI testing (e.g. performance testing).
   Defaults to `False`.
* `send_report_to_upstream`: `True` if the email report should be sent to the
   contributors. Causes the triggers to add emails to the `mail_to`, `mail_cc`
   and `mail_bcc` trigger variables and the reporter to send the email to them.
  **NOTE** recipient handling is being reworked and this variable will be
  removed later.
* `server_url`: Value of the `--server` option of the `koji` command to use to
   retrieve builds. Required for Brew/Koji pipelines and for COPR pipelines
   using `download_separate_headers`.
* `skip_beaker`: `True` if only a dry run for submitting a test job is needed.
   Defaults to `True` in CI runs, to `False` otherwise.
* `skip_build`: `True` if the build stage should be skipped in the pipeline.
   Useful for only running testing with already existing kernels. Defaults to
   `False`.
* `skip_createrepo`: `True` if the createrepo stage should be skipped in the
   pipeline. Useful for only running testing with already existing kernels.
   Defaults to `False`.
* `skip_kernel_installation`: `True` if the kernel installation task
   (Boot test - kpkginstall) should be skipped. Useful for external provisioner
   or systems which have a preconfigured kernel already installed. Defaults to
   `False`.
* `skip_merge`: `True` if the merge stage should be skipped in the pipeline.
   Useful for only running testing with already existing kernels. Defaults to
   `False`.
* `skip_publish`: `True` if the publish stage should be skipped in the
   pipeline. Useful for only running testing with already existing kernels.
   Defaults to `False`.
* `skip_results`: `True` if result summary and known issue detection should be
   disabled. Defaults to `False`.
* `skip_setup`: `True` if the setup stage should be skipped. Defaults to
   `False`.
* `skip_test`: `True` if the test stage should be skipped. Defaults to `False`.
* `srpm_make_target`: `make` target to build SRPM. Defaults to `rh-srpm`.
* `subject`: Email subject to use with email reports for the test results, will
   be prefixed with the result summary. If missing, a generic subject
   mentioning the kernel version and pipeline name will be built.
* `submitter`: Email of the patch or build submitter.
* `test_debug`: `True` if the debug kernels should also be tested. Defaults to
   `False`. Only `x86_64` debug builds and tests are implemented.
* `test_priority`: Priority for submitting test jobs. Can be `low`, `medium`,
   `normal`, `high` or `urgent`. Defaults to `normal`. **NOTE** not every
   provisioner supports test run priorities. **USE RESPONSIBLY**.
* `test_runner`: Where to run testing. Currently available runners are `aws`
   (limited testing available in virtual machines) and `beaker`. Defaults to
   `beaker`.
* `test_set`: Test set(s) to run for the pipeline, in the [kpet generate] `-s`
   option syntax. By default, all sets are included for patch runs (and tests
   are picked via patch analysis) and official kernel builds are limited to
   sanity `kt0` testing. Also check out documentation for targeted testing for
   [builds] on how to limit test sets for a single build.
* `tests_regex`: If specified, tests names have to `fullmatch()` this regular
   expression to run. Can be combined with `test_set` to only run specific tests
   from the set(s). **Not meant for production use as the test names can change,
   always use `test_set` for production.** Current list of test names can be
   retrieved by running [kpet test list].
* `test_upt`: `True` if the pipeline is for running UPT smoke tests.
   Defaults to `False`, i.e. generate the Beaker XML with kpet.
* `top_url`: Value of the `--topurl` option of the `koji` command to use to
   retrieve builds. Required for Brew/Koji pipelines and for COPR pipelines
   using `download_separate_headers`.
* `trigger_job_name`: The `CI_JOB_NAME` which triggered the pipeline. Useful
   for consumers of downstream pipeline webhook events such as the
   kernel-webhook's ckihook. A regular downstream pipeline is expected to be
   named using the following format: `<RHELMAJOR>_<VARIANT>_<TYPE>_<SECLEVEL>`:
  * `<RHELMAJOR>`: RHEL major version, i.e. `rhel8` or for CentOS Stream `c9s`,
    `c10s`, etc.
  * `<VARIANT>`: optional and can be `automotive`, `realtime` or
     `realtime[_-]check`
  * `<TYPE>`: `baseline` or `merge[_-]request`
  * `<SECLEVEL>`: optional; only `private` is a valid value

  There are also special coverage and shadow builds enabled for specific setups.
  These conform to the schema of `<RHELMAJOR>_<VARIANT>_baseline_coverage_build`
  and `<CENTOSMAJOR>_<RHELMAJOR>_compat_<TYPE>`. Note that the `<RHELMAJOR>` in
  case `<CENTOSMAJOR>` is used as well matches the RHEL name, *not* the CentOS
  Stream name.
* `upt_smoke_tests`: Space-separated list of UPT smoke tests to run.
   Defaults to all available smoke tests (See [pipeline variables]).
   Has effect only if `test_upt` is `True`.
* `watch_branch` Git branch to watch to trigger testing on changes. Defaults
   to `branch`. For use with [baseline trigger] only.
* `watch_url`: Git repo URL to watch to trigger testing on changes. Defaults
   to `git_url`. For use with [baseline trigger] only.
* `web_url`: Value of the `--weburl` option of the `koji` command to use to
   retrieve builds. Required for Brew/Koji pipelines and for COPR pipelines
   using `download_separate_headers`.

[pipeline triggers]: https://gitlab.com/cki-project/pipeline-trigger
[share the same code]: https://gitlab.com/cki-project/pipeline-definition/-/blob/main/cki_pipeline.yml
[kpet tree selection script]: https://gitlab.com/cki-project/cki-tools/-/blob/main/cki/cki_tools/select_kpet_tree.py
[living in GitLab]: gitlab-mr-testing/
[Data Warehouse]: https://gitlab.com/cki-project/datawarehouse
[boot test]: https://gitlab.com/cki-project/kernel-tests/-/tree/main/distribution/kpkginstall
[UPT]: https://gitlab.com/cki-project/upt
[UMB messenger]: https://gitlab.com/cki-project/umb-messenger
[kpet generate]: https://gitlab.com/cki-project/kpet#generate
[builds]: targeted-brew.md
[kpet test list]: https://gitlab.com/cki-project/kpet#list-1
[kpet-db]: https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db/-/blob/main/index.yaml
[baseline trigger]: https://gitlab.com/cki-project/pipeline-trigger/-/blob/main/triggers/baseline_trigger.py
[pipeline variables]: https://gitlab.com/cki-project/pipeline-definition/-/blob/main/pipeline/variables.yml

---
title: Adding a new Kubernetes deployment context
linkTitle: New k8s context
description: >
    How to enable Kubernetes deployments to a new cluster and/or namespace in
    deployment-all
---

## Problem

A new Kubernetes context needs to be enabled in [deployment-all].

## Steps

1. Verify that the `deployment-all` checkout works correctly:

   ```bash
   ./openshift_login.sh
   ```

1. Login to the cluster with admin rights, set the default namespace and show
   the context name (`EXTERNAL_CONTEXT`) via

   ```bash
   KUBECONFIG= oc login https://K8S.API.URL:6443 --token=TOKEN
   KUBECONFIG= oc config set-context --current --namespace=NAMESPACE
   KUBECONFIG= oc config current-context  # Save as EXTERNAL_CONTEXT, used later
   ```

   For OpenShift, the token can be obtained after logging into the web interface
   by clicking on your name in the top-right corner and selecting `Copy login
   command`.

1. Add the new context to the `KUBERNETES_CREDENTIALS` variable in `secrets.yml`.

   ```yaml
     NEW_CONTEXT:  # name of new context
       server: https://K8S.API.URL:6443
       namespace: NAMESPACE
       token: OPENSHIFT_NEW_CONTEXT_KEY  # env var for new sa token
       pvcmode: ReadWriteOnce  # RWO for AWS/EBS, RWX if possible for EFS/NFS
       dns_record_type: cname  # 'cname' for AWS, 'a' everywhere else
   ```

   Import the secrets again and deploy the setup project from
   `openshift/setup` via

   ```shell
   ./openshift_setup_context.sh NEW_CONTEXT EXTERNAL_CONTEXT
   ```

1. Edit `secrets.yml` and move the new token env variable
   (`OPENSHIFT_NEW_CONTEXT_KEY`) to the appropriate place.

1. Edit `.gitlab-ci.yml` and add the new context to the deployment of the
   `setup` project.

[deployment-all]: https://gitlab.cee.redhat.com/cki-project/deployment-all

---
title: "CKI-001: CKI feedback mechanism"
linkTitle: CKI-001
description: |
    Description of the process behind the CKI feedback mechanism based on
    Requests for Comments (RFCs)
layout: rfc
author: Michael Hofmann
mr: 49
---

## Abstract

This document specifies the process behind the feedback mechanism for the CKI
project based on Request for Comments (RFCs). Each RFC documents a *need*,
proposed *solutions* and links to the related *discussions*.

## Motivation

The internal Red Hat `#kernelci` IRC channel is the place where nearly all CKI
project communication happens. Unless a project member is online, logged into IRC
and paying attention all the time, ad-hoc discussion of important topics might
be missed. As a consequence, people might feel left out of the decision-making
process, and proposed solutions might suffer from the lack of feedback.

## Approach

A structured process for gathering feedback is introduced.

CKI RFCs (“Requests for Comments”) are markdown documents proposing to create
or change something, and soliciting discussion and feedback. They live in the
[documentation repository] and can be browsed at
<https://cki-project.org/docs/rfcs/>. They are submitted and discussed
via merge requests. Within the default time frame of one week, everyone is
invited to give feedback on them.

Submitting RFCs is **not required**, but all CKI project members are
**encouraged** to submit RFCs for proposals that affect the whole project,
or where they want to gather feedback on how to proceed.

Submitting RFCs is **not blocking** the implementation. If possible, the
implementation of the proposal should only be started after the feedback has
been collected, but this is not mandatory.

### Steps to submit a new RFC

1. Fork and clone the [documentation repository] via

   ```bash
   # fork using the GitLab UI
   git clone https://gitlab.com/your-user-name/documentation
   git submodule update --init --recursive
   ./update-inventory.sh
   ```

   The [README.md] file contains instructions on how to preview the rendered
   documentation.

1. Create a new RFC markdown document in `content/docs/rfcs` via

   ```bash
   cp content/docs/rfcs/cki-000-template.md content/docs/rfcs/cki-XXX-description.md
   ```

   Replace XXX by the next unused RFC number.

1. Fill in the RFC document:

   - Motivation: Why is this RFC needed?
   - Approach: What is the proposed solution? Which steps are needed?
   - Benefits: What is the expected outcome?
   - Drawbacks: Why should we not do this?
   - Alternatives: Which alternative approaches are available?

1. Prefix the commit description with `[RFC]`. Push it to a branch in your
   fork, and create a new merge request. Update the merge request number in the
   metadata of the document. Mark the merge request as `Draft`.

1. When you are happy with the RFC draft, mark the merge request as ready by
   removing the `Draft` from the title.

1. Assign required reviewers and ping all CKI project members by mentioning
   `@cki-project` in a comment.

1. Everybody has one week from RFC submission to provide feedback and discuss
   the proposal in the merge request. If needed, this time frame can be
   extended. The authors incorporate the feedback into the proposal.

1. The merge request is merged.

## Benefits

The described feedback process has multiple benefits:

- the RFC template helps the authors to explain their proposal in a structured
  way and makes giving feedback easier
- it provides a clear way to discuss a proposal with all CKI project members
- it allows to access more points of view and create awareness of the topic and
  proposal

The process will also improve the way the CKI project operates as an
environment "where the best ideas win". If the authors care about a topic enough
to create an RFC document, they should be trusted to do the right thing.

On the one hand, with the feedback gathered via the described process, the
authors should be able to continue further with the proposed or a modified
solution, or abandon it. It is their decision. They don't need to wait for
everyone's confirmation of their proposal.

On the other hand, authors will be held accountable. They have to follow up on
their proposal and see it to completion. That does not mean that they will be
blamed on failure.

## Drawbacks

The described process is only able to gather feedback on a proposal. It is not
suited to reach consensus on contentious topics.

## Alternatives

Feedback could also be gathered via

- ad-hoc Etherpads
- dedicated time slots for discussions either on IRC or in-person
- GitLab issues

All of the above would result in a less structured way for gathering feedback.

## References

- <https://candost.blog/how-to-stop-endless-discussions/>

[documentation repository]: https://gitlab.com/cki-project/documentation
[README.md]: https://gitlab.com/cki-project/documentation/-/blob/main/README.md

<!-- vi: set spell spelllang=en: -->

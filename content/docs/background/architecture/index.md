---
title: "CKI architecture"
description: >
  High-level description of the CKI setup and its architecture
---

In a nutshell, the CKI setup consists of the following components:

- the GitLab pipeline and connected testing lab
- the DataWarehouse GUI for result visualization and triaging
- various micro services that mediate the dataflow around them

![High-level architecture](cki-overview.png)

The following sections describe the various parts in more detail.

## Pipeline

### Triggering from merge requests

For merge requests on [gitlab.com], the CKI pipeline is triggered as a
[multi-project downstream pipeline][multi-project-pipeline].

An example what this looks like in practice can be seen in the `.gitlab-ci.yml`
file in the [CentOS Stream 9][cs9-kernel] kernel repository. With the templates
for the trigger jobs as defined in the `kernel_templates.yml` file in the
[pipeline-definition repository][job-templates], the trigger jobs eventually
boil down to something like

```yaml
c9s_merge_request:
  trigger:
    project: redhat/red-hat-ci-tools/kernel/cki-internal-pipelines/cki-trusted-contributors
    branch: c9s
    strategy: depend
  variables:
    ...
```

### Triggering from Koji and Copr

Official [Fedora][koji-fedora], [CentOS Stream][koji-cs] and RHEL kernels are
built using the [Koji RPM building and tracking system][koji]. Similarly, the
[Copr build system][copr] allows anybody to build and host custom RPMs for
Fedora, CentOS Stream or RHEL.

When new kernel RPMs have been built, CKI testing is triggered via the
`brew_trigger` module in the [pipeline-trigger repository][brew-trigger].

### Triggering from git repositories

For Git repositories, e.g. on [git.kernel.org], CKI testing can be triggered
whenever the HEAD commit of a branch changes. This is implemented via a regular
cron job calling into the `baseline_trigger` module in the [pipeline-trigger
repository][baseline-trigger].

### Triggering via the CKI CI Bot

To test CKI code changes before they are deployed to production, the
`gitlab_ci_bot` module in the [cki-tools repository][ci-bot] allows to [trigger
canary pipelines][bot-example] with the new code from a merge request. The bot
is implemented as a cron job and is present in all CKI projects that are used
in the GitLab pipeline. As the canary pipelines running new code are based on
previously successful pipelines, they are expected to complete successfully as
well.

### GitLab pipeline

The GitLab pipelines run in the various branches of the [CKI pipeline
projects][pipeline-projects]. The actual pipeline code comes from the
[pipeline-definition repository][pipeline-code] and is included via the
`.gitlab-ci.yml` file in the pipeline projects like

```yaml
include:
  - project: cki-project/pipeline-definition
    ref: production
    file: cki_pipeline.yml
```

Based on the specific [trigger variables][trigger-variables] of the triggered
pipelines, the jobs are configured appropriately for the kernel code under test.

### Testing lab

The actual testing of the kernels happens outside the GitLab pipeline in a
[Beaker lab][beaker] via [upt]. The [kpet-db repository] hosts the database
with the information to select the tests that should be run. The GitLab
pipeline waits for testing to finish before continuing.

### Transient failure detection

For the GitLab pipeline, network hiccups or upstream server issues might result
in transient job failures that are not connected to the code under test. Next
to retrying network requests wherever possible, the [pipeline-herder]
checks failed jobs for signs of known transient problems. In that case, the
failed job is automatically restarted.

For successful jobs or failed jobs where no such problems could be detected,
the pipeline-herder hands the results to the next stage.

## Result processing

### Result submission

Results are prepared in GitLab pipeline jobs in [KCIDB format][kcidb]. If jobs
pass the pipeline-herder, the [datawarehouse-submitter] submits these results
to [DataWarehouse][dw].

### DataWarehouse result visualization and triage

The [DataWarehouse][dw] provides access to all CKI testing results.
Additionally, it allows to manage known issues, i.e. failures and errors that
are tracked in an external issue tracker.

### Automatic DataWarehouse result triaging

Known issues that result in specific error messages in the log files can also
be automatically tagged based on regular expressions. The
[datawarehouse-triager] is responsible for the tagging of new results and the
updating of old ones should the regular expressions change.

## Reporting

Continuously, the [kcidb-forwarder] takes care of forwarding incoming KCIDB
results to the upstream [KernelCI] project. When all testing is finished, the
[umb-messenger] sends UMB messages to allow kernel packages to pass through
gating. The [reporter] is responsible for sending email reports, mostly to
upstream mailing lists.

[gitlab.com]: https://gitlab.com
[multi-project-pipeline]: https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#multi-project-pipelines
[cs9-kernel]: https://gitlab.com/redhat/centos-stream/src/kernel/centos-stream-9/-/blob/main/.gitlab-ci.yml
[job-templates]: https://gitlab.com/cki-project/pipeline-definition/-/blob/main/kernel_templates.yml
[koji]: https://pagure.io/koji/
[koji-fedora]: https://koji.fedoraproject.org/koji/
[koji-cs]: https://kojihub.stream.centos.org/koji/
[brew-trigger]: https://gitlab.com/cki-project/pipeline-trigger/-/blob/main/triggers/brew_trigger.py
[copr]: https://copr.fedorainfracloud.org/
[git.kernel.org]: https://git.kernel.org/
[baseline-trigger]: https://gitlab.com/cki-project/pipeline-trigger/-/blob/main/triggers/baseline_trigger.py
[ci-bot]: https://gitlab.com/cki-project/cki-tools/-/blob/main/cki/cki_tools/gitlab_ci_bot.py
[bot-example]: https://gitlab.com/cki-project/pipeline-definition/-/merge_requests/1749#note_1259556575
[pipeline-projects]: https://gitlab.com/redhat/red-hat-ci-tools/kernel
[pipeline-code]: https://gitlab.com/cki-project/pipeline-definition/-/blob/main/cki_pipeline.yml
[trigger-variables]: ../../user_docs/configuration.md
[beaker]: https://beaker-project.org/
[upt]: https://gitlab.com/cki-project/upt
[kpet-db repository]: https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db
[pipeline-herder]: https://gitlab.com/cki-project/cki-tools/-/tree/main/cki_tools/pipeline-herder
[kcidb]: https://github.com/kernelci/kcidb-io
[datawarehouse-submitter]: https://gitlab.com/cki-project/cki-tools/-/blob/main/cki/kcidb/datawarehouse_submitter.py
[dw]: https://datawarehouse.cki-project.org/
[datawarehouse-triager]: https://gitlab.com/cki-project/cki-tools/-/tree/main/cki/triager
[KernelCI]: https://kernelci.org/
[reporter]: https://gitlab.com/cki-project/reporter
[kcidb-forwarder]: https://gitlab.com/cki-project/cki-tools/-/blob/main/cki/kcidb/forward_upstream.py
[umb-messenger]: https://gitlab.com/cki-project/umb-messenger

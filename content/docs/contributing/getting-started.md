---
title: "Getting started"
linkTitle: "Getting started"
description: How to start contributing to the code of the CKI project
weight: 10
---

## Red Hat associate

If you are a Red Hat associate getting started on the CKI team, start with the
[internal onboarding documentation].

## Development setup

### Linux distribution

To start contributing, you don't need any specific Linux distribution. While
we encourage everyone to develop on [Fedora Workstation] or [Fedora
Silverblue], this is not a requirement. Where Fedora/RHEL-specific software is
needed, container-based alternative workflows are provided.

### Python version

To use CKI projects you must install at least Python 3.9, but you're encouraged
to use the latest stable version.

### Install dependencies

We need to install [direnv], [tox] and [git].

#### Fedora Workstation

All should be packaged for most distributions and
be installable with something like

```bash
sudo dnf install tox direnv git
```

#### Red Hat 8

If you using RHEL 8 or RHEL 8 CSB, please take the following in consideration:

* Git can easily be installed

  ```bash
  dnf install git
  ```

* Default python3 version in RHEL 8 is 3.6. Update to Python 3.9, more details on [official documentation].

  ```bash
  sudo dnf install python39
  ```

* Tox and direnv are not available via dnf.

  ```bash
  pip3 install tox
  curl -sfL https://direnv.net/install.sh | bash
  ```

### Git config

Configure a global .gitignore file and add entries for direnv to it via
something like

```bash
IGNORE_FILE=$HOME/.config/git/excludes
git config --global core.excludesfile "${IGNORE_FILE}"
mkdir -p "${IGNORE_FILE%/*}"
echo "/.envrc" >> "${IGNORE_FILE}"
echo "/.direnv/" >> "${IGNORE_FILE}"
```

### Cloning CKI repositories and creating forks

For both GitLab instances, create `api` GitLab personal access tokens. The
tokens are going to be stored in the `COM_GITLAB_TOKEN_PERSONAL` and
`CEE_GITLAB_TOKEN_PERSONAL` environment variables below.

The following bash commands temporarily bootstrap cki-tools, and then use the
included `repo-manager` module to clone all repositories including forks and
setup pip:

<!-- markdownlint-disable line-length -->
```bash
mkdir ~/git-temp ~/git-cki
cd ~/git-temp
echo "export GITLAB_TOKENS='{\"gitlab.com\":\"COM_GITLAB_TOKEN_PERSONAL\", \"gitlab.cee.redhat.com\":\"CEE_GITLAB_TOKEN_PERSONAL\"}'" > .envrc
echo 'export COM_GITLAB_TOKEN_PERSONAL="your-secret-token-from-gitlab-com"' >> .envrc
echo 'export CEE_GITLAB_TOKEN_PERSONAL="your-secret-token-from-gitlab-cee"' >> .envrc
cp .envrc ~/git-cki/
git clone git@gitlab.com:cki-project/cki-tools
cd cki-tools
printf 'layout python3\nsource_up\n' > .envrc
direnv allow
python3 -m pip install -e .
python3 -m cki.cki_tools.repo_manager --directory ~/git-cki --fork fork  --venv --force
cd ~/git-cki
rm -rf ~/git-temp
```
<!-- markdownlint-restore -->

### Linting and tests

#### Python-based projects

Linting, unit tests and code coverage checks are implemented in [cki\_lint.sh]
and can be invoked in a clean environment by running

```bash
tox
```

You can pass arguments to the `pytest` executable with the environment
variable `PYTEST_ARGS`.  For example, the following code runs only on
`test_name` in `tests/test_file_name.py`:

```bash
TOX_TESTENV_PASSENV=PYTEST_ARGS \
PYTEST_ARGS="tests/test_file_name.py::class_name::test_name" \
tox 
```

To run tox with a different Python version, explicitly specify the Python
interpreter via

```bash
python3.9 -m tox
```

In the case of host problems, e.g. if no venv is available, the Python version
is too old or certificates are not setup correctly, tox can also be run in a
container via

```bash
podman run --rm --workdir /code --volume .:/code \
    quay.io/cki/cki-tools:production \
    tox --workdir .tox-container
```

To run only in a subset of the tests use `PYTEST_ARGS`:

```bash
podman run --rm --workdir /code --volume .:/code \
    --env TOX_TESTENV_PASSENV=PYTEST_ARGS \
    --env PYTEST_ARGS="tests/test_file_name.py[::class_name[::test_name]] ..." \
    quay.io/cki/cki-tools:production \
    tox --workdir .tox-container
```

Individual tests can also be run directly on the host via any of

```bash
python3 -m unittest tests.test_module
python3 -m unittest tests/test_file_name.py
pytest tests/test_file_name.py
```

The [cki-lib README.md] file contains the parameters for correct editor
integration of linters/fixers for CKI code.

#### Pipeline code

In [com/pipeline-definition], the linting and testing can be invoked locally via

```bash
./lint.sh
./tests.sh
```

or in a container via

```bash
podman run --rm --workdir /code --volume .:/code \
    quay.io/cki/cki-tools:production \
    ./lint.sh
podman run --rm --workdir /code --volume .:/code \
    quay.io/cki/builder-rawhide:production \
    ./tests.sh
```

[internal onboarding documentation]: https://documentation.internal.cki-project.org/docs/contributing/onboarding/
[Fedora Workstation]: https://getfedora.org/nl/workstation/
[Fedora Silverblue]: https://silverblue.fedoraproject.org/
[official documentation]: https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_basic_system_settings/assembly_installing-and-using-python_configuring-basic-system-settings
[direnv]: https://direnv.net/
[git]: https://git-scm.com/
[tox]: https://tox.readthedocs.io/en/latest/
[cki\_lint.sh]: https://gitlab.com/cki-project/cki-lib/-/blob/main/cki_lint.sh
[cki-lib README.md]: https://gitlab.com/cki-project/cki-lib/-/blob/main/README.md
[com/pipeline-definition]: https://gitlab.com/cki-project/pipeline-definition

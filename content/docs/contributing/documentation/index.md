---
title: "Writing documentation"
linktitle: Documentation
description: How to contribute to the documentation
weight: 30
---

New documentation is always welcome!

Start by cloning the documentation repository and installing the necessary
requirements:

```bash
git clone --recurse-submodules \
    https://gitlab.com/cki-project/documentation \
    documentation
cd documentation
make setup
```

This requires [Hugo] to be installed via the system package manager. Instead,
it is also possible to execute all of the commands in the shell of a container
with everything preinstalled via `make podman`.

Add new documentation as markdown files inside the appropriate directory
under `content/`. Every file should end in `.md`. The repositories use [Hugo]
with the [Docsy] theme to build the site.

Documentation can be previewed by running `make serve`. A local web server will
start and automatically refresh the preview each time a change is saved.

Available tests can be executed by running `make test`.

To submit changes, fork the repository, add the fork as a remote to the
checkout and commit the changes. Push them to a new branch in the fork, and
start a merge request against the parent.

## Internal repository

The CKI documentation is **open by default** and maintained via the [public
repository][public-repo]. Additionally, an internal repository
exists to be able to provide further confidential
information.

The contents of these two repositories are used to generate two different
versions of the documentation:

- the [public documentation][public-docs], solely based on information from the
  [public repository][public-repo]
- the [internal documentation][internal-docs], based on information from the
  [public repository][public-repo], supplemented by confidential details about
  internal infrastructure from the internal documentation repository

The CKI deployment bot is configured to redeploy the internal documentation
when changes to the public repository are merged.

To contribute to the internal repository, install some additional dependencies,
clone the internal documentation repository next to the public repository, and
link both checkouts via

```bash
sudo dnf install inotify-tools stow
git clone --recurse-submodules \
    https://internal/documentation/repository.git \
    internal-documentation
cd internal-documentation
mkdir stow
ln -s ../../documentation stow/public
./build.sh && cd build
make setup
```

The `build.sh` script uses [stow] to interleave files from the public and
internal repositories, with files from the internal repository taking
precedence. This allows the internal repository to add or overwrite
files from the public repository.

Run `make serve` in the `build` directory to start a local web server that
serves the merged documentation.

## Adding confidential information

With Hugo, content is organized into [page bundles].

As an example, consider the following structure in the public repository:

```plain
documentation/content/
  \- _index.md
  \- page1.md
  \- page2/
     \- index.md
```

This will generate three pages: an index page for the top-level `_index.md`,
and two content pages for `page1.md` and `page2/index.md`.

To include confidential content from additional Markdown files supplied via the
internal repository:

1. In the public repository, move content Markdown files in branch bundles
   (directories with a `_index.md` file) into separate leaf bundles. In the
   example, this means moving `page1.md` into a separate directory:

   ```plain
   documentation/content/
     \- _index.md
     \- page1/
        \- index.md
     \- page2/
        \- index.md
   ```

1. In the internal repository, add the confidential content as a separate
   Markdown file in the appropriate page bundle:

   ```plain
   internal-documentation/content/
     \- page1/
        \- confidential-section.md
   ```

   This is what `content/page1/confidential-section.md` in the internal
   repository could look like:

   ```plain
   ---
   ---
   <!-- markdownlint-disable first-line-heading -->
   ## Confidential section

   Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
   tempor incididunt ut labore et dolore magna aliqua. ...
   ```

1. Reference the file via the `include` shortcode in `content/page1/index.md`
   in the public repository:

   ```plain
   ...
   {{%/* include "confidential-section.md" */%}}
   ...
   ```

   When the public documentation is rendered, the missing include is ignored
   gracefully.

{{% include "internal.md" %}}

[public-repo]: https://gitlab.com/cki-project/documentation/

[public-docs]: https://cki-project.org/
[internal-docs]: https://documentation.internal.cki-project.org/

[stow]: https://www.gnu.org/software/stow/
[page bundles]: https://gohugo.io/content-management/page-bundles/

[Hugo]: https://gohugo.io/
[Docsy]: https://www.docsy.dev/

#!/bin/bash

set -euo pipefail

# Read data and replace newlines with comas
excludes=$(cat check-links-*.txt | tr '\n' ',')

# Run the checker
urlchecker check . \
    --file-types "*.md" \
    --subfolder content/ \
    --exclude-pattern "${excludes}" \
    --timeout 60 \
    --retry-count 5 \
    --no-print
